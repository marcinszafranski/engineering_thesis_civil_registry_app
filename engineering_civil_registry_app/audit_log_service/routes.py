from flask import request, jsonify
from models import AuditLog
from app import app, db


@app.route('/log_audit', methods=['POST'])
def log_audit():
    data = dict(request.get_json())
    new_audit_log = AuditLog(
        person_id=int(data['person_id']),
        caller=str(data['caller']),
        action=str(data['action']),
        input=str(data['input'])
    )
    db.session.add(new_audit_log)
    db.session.commit()

    return jsonify({"message": "Audit log recorded successfully"})


@app.route("/get-logs", methods=['GET'])
def get_logs():
    person_id = request.args.get('person_id')
    audit_logs = AuditLog.query.filter_by(person_id=person_id)
    result = []
    for audit_log in audit_logs:
        audit_log_data = {
            "timestamp": audit_log.timestamp,
            "person_id": audit_log.person_id,
            "caller": audit_log.caller,
            "action": audit_log.action,
            "input": audit_log.input
        }
        result.append(audit_log_data)
    return jsonify(result), 200
