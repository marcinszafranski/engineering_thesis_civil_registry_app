from datetime import datetime
from sqlalchemy import Sequence
from flask_login import UserMixin
from app import db


class AuditLog(db.Model):
    __tablename__ = 'audit_logs'
    id = db.Column(db.Integer, Sequence('audit_id_seq'), primary_key=True)
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)
    person_id = db.Column(db.Integer)
    caller = db.Column(db.String)
    action = db.Column(db.String)
    input = db.Column(db.String)


class User(db.Model, UserMixin):
    __tablename__ = 'users'
    id = db.Column(db.Integer, Sequence('user_id_seq'), primary_key=True)
    name = db.Column(db.String(64))
    surname = db.Column(db.String(64))
    pesel = db.Column(db.String(11), unique=True, index=True)
    username = db.Column(db.String(64), unique=True, index=True)
    password_hash = db.Column(db.String(128))
    birth_date = db.Column(db.Date)
    role_id = db.Column(db.Integer, nullable=False)
    is_activated = db.Column(db.Integer, default=0)