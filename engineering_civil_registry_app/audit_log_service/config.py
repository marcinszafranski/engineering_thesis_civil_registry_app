import os
import json


class Config:

    @staticmethod
    def get_connection_string():
        with open(os.environ['CONF_FILE'], 'r') as conf_file:
            creds = json.load(conf_file)

        user = creds['USER']
        password = creds['PASSWORD']
        dsn = creds['DSN']
        config_dir = creds['CONFIG_DIR']
        wallet_location = creds['WALLET_LOCATION']
        wallet_password = creds['WALLET_PASSWORD']

        return f'oracle+oracledb://{user}:{password}@{dsn}?wallet_location={wallet_location}&config_dir={config_dir}' \
               f'&wallet_password={wallet_password}'

    SECRET_KEY = os.environ['SECRET_KEY']
    SQLALCHEMY_DATABASE_URI = get_connection_string()
    SQLALCHEMY_TRACK_MODIFICATIONS = False
