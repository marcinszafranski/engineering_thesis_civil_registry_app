from flask import render_template, url_for, redirect, flash, request, Response
from flask_login import login_required, current_user
from app import app
from models import User
from forms import RemoveEventForm, SearchUsersForm, RecoveryDataForm, AuditLogForm
import requests
from utils import Utils

SERVICE_NAME = "AGGREGATION_SERVICE"
utils = Utils(SERVICE_NAME)


@app.route('/contact')
@login_required
def contact():
    return render_template('contact.html', user_role=current_user.role_id)


@app.route('/about')
@login_required
def about():
    return render_template('about.html', user_role=current_user.role_id)


@app.route('/registry-search', methods=['GET', 'POST'])
@login_required
def registry_search():
    events = utils.retrieve_events(current_user.id, 0)
    return render_template('search.html', user_role=current_user.role_id, person_id=current_user.id, events=events)


@app.route('/download-document', methods=['GET', 'POST'])
@login_required
def download_document():
    document_id = request.args.get('document_id')

    # Fetch the document from the external service (assuming it's running on port 5002)
    document_url = f"http://localhost:5002/get-document?document_id={document_id}"
    response = requests.get(document_url)
    status_code = response.status_code

    variables = {'document_id': document_id, 'status_code': status_code}
    audit_log_data = {
        "person_id": current_user.id,
        "caller": SERVICE_NAME,
        "action": utils.retrieve_function_name(),
        "input": variables
    }
    utils.register_action(audit_log_data)
    # Check if the document was found and exists
    if status_code == 200:
        # Set the appropriate headers to trigger the file download
        headers = {
            "Content-Disposition": f"attachment; filename=document_{document_id}.pdf",
            "Content-Type": "application/pdf",
        }
        # Use Response to send the file with the appropriate headers
        return Response(response.content, headers=headers)
    else:
        flash('Error downloading document', 'error')
        return redirect(url_for('registry_search'))


@app.route("/add-event")
@login_required
def add_event():
    utils.check_access()
    return render_template("add_event.html", user_role=current_user.role_id)


@app.route('/create-event', methods=['GET', 'POST'])
@login_required
def create_event():
    utils.check_access()
    if request.method == 'POST':
        event_type_id = request.form.get('event_type')
        person_id = request.form.get('person_id')
        event_date = request.form.get('event_date')
        event_description = request.form.get('event_description')
        second_person_id = request.form.get('second_person_id')
        is_archived = 0
        event_document = request.files['event_document']  # Get the uploaded file

        data = {
            'event_type_id': event_type_id,
            'person_id': person_id,
            'second_person_id': second_person_id,
            'event_date': event_date,
            'event_description': event_description,
            'is_archived': is_archived
        }

        files = {'event_document': (event_document.filename, event_document)}  # Use the uploaded file

        response = requests.post('http://localhost:5002/create-event', files=files, data=data)
        status_code = response.status_code

        variables = {'id': person_id, 'status_code': status_code}
        variables.update(data)
        audit_log_data = {
            "person_id": current_user.id,
            "caller": SERVICE_NAME,
            "action": utils.retrieve_function_name(),
            "input": variables
        }
        utils.register_action(audit_log_data)

        if response.status_code == 200:
            flash('Zdarzenie zostało pomyślnie utworzone!', 'success')
        else:
            flash('Wystąpił błąd podczas tworzenia zdarzenia', 'error')

    return redirect(url_for('add_event'))


@app.route('/archive-event', methods=['GET', 'POST'])
@login_required
def archive_event():
    utils.check_access()
    event_id = request.args.get('event_id')
    archive_url = f"http://localhost:5002/archive-event?event_id={event_id}"
    response = requests.get(archive_url)
    status_code = response.status_code


    variables = {'event_id': event_id, 'status_code': status_code}
    audit_log_data = {
        "person_id": current_user.id,
        "caller": SERVICE_NAME,
        "action": utils.retrieve_function_name(),
        "input": variables
    }
    utils.register_action(audit_log_data)

    if status_code == 200:
        flash('Event removed successfully', 'success')
    else:
        flash('Error removing event', 'error')
    return redirect(url_for('remove_event'))


@app.route('/unarchive-event', methods=['GET', 'POST'])
@login_required
def unarchive_event():
    utils.check_access()
    utils.check_read_only_access()
    event_id = request.args.get('event_id')
    archive_url = f"http://localhost:5002/unarchive-event?event_id={event_id}"

    response = requests.get(archive_url)
    status_code = response.status_code
    variables = {'event_id': event_id, 'status_code': status_code}
    audit_log_data = {
        "person_id": current_user.id,
        "caller": SERVICE_NAME,
        "action": utils.retrieve_function_name(),
        "input": variables
    }
    utils.register_action(audit_log_data)

    if status_code == 200:
        flash('Zdarzenie przywrócone pomyślnie', 'success')
    else:
        flash('Problem podczas przywracania zdarzenia', 'error')
    return redirect(url_for('remove_event'))


@app.route('/access-control', methods=['GET', 'POST'])
@login_required
def access_control():
    utils.check_access()
    utils.check_read_only_access()
    return render_template('access_control.html', user_role=current_user.role_id)


@app.route('/activate-user', methods=['GET', 'POST'])
@login_required
def activate_user():
    utils.check_access()
    utils.check_read_only_access()
    person_id = request.args.get('person_id')
    user_permissions = request.args.get('user_permissions')
    if person_id:
        if request.args.get('activate_account') == "on":
            activate_account_url = f"http://localhost:5003/activate-user?person_id={person_id}"
            response = requests.post(activate_account_url)
            status_code = response.status_code

            variables = {'id': person_id, 'status_code': status_code}
            audit_log_data = {
                "person_id": current_user.id,
                "caller": SERVICE_NAME,
                "action": utils.retrieve_function_name(),
                "input": variables
            }
            utils.register_action(audit_log_data)

            if status_code == 200:
                flash("Pomyślnie aktywowano konto", 'success')
            else:
                flash("Aktywowanie konto nie powiodło się", "error")
        change_role_url = f"http://localhost:5003/change-role?person_id={person_id}&user_permissions={user_permissions}"
        response = requests.post(change_role_url)
        if response.status_code == 200:
            flash("Pomyślnie nadano uprawnienia", 'success')
        else:
            flash("Nadanie uprawnień nie powiodło się", "error")
    return redirect(url_for('access_control'))


@app.route('/search-users', methods=['GET', 'POST'])
@login_required
def search_users():
    utils.check_access()
    form = SearchUsersForm()
    person_id = form.person_id.data
    pesel = form.pesel.data
    name_surname = form.name_surname.data
    if name_surname:
        name, surname = name_surname.split(" ")
    if person_id:
        user = User.query.filter_by(id=person_id).all()
    elif name_surname:
        user = User.query.filter_by(name=name, surname=surname).all()
    else:
        user = User.query.filter_by(pesel=pesel).all()
    if user:
        events = []
        for u in user:
            variables = {'id': u.id}
            audit_log_data = {
                "person_id": current_user.id,
                "caller": SERVICE_NAME,
                "action": utils.retrieve_function_name(),
                "input": variables
            }
            utils.register_action(audit_log_data)
            events += utils.retrieve_events(u.id, 10)
        return render_template('search_users.html', user_role=current_user.role_id, events=events, form=form)
    else:
        return render_template('search_users.html', user_role=current_user.role_id, form=form)


@app.route('/recovery-data', methods=['GET', 'POST'])
@login_required
def recovery_data():
    utils.check_access()
    utils.check_read_only_access()
    form = RecoveryDataForm()
    user = User.query.filter_by(id=form.person_id.data).first()
    variables = {'id': form.person_id.data}
    if user:
        audit_log_data = {
            "person_id": current_user.id,
            "caller": SERVICE_NAME,
            "action": utils.retrieve_function_name(),
            "input": variables
        }
        utils.register_action(audit_log_data)
        events = utils.retrieve_events(user.id, 1)
        return render_template('recovery_data.html', user_role=current_user.role_id, events=events, form=form)
    else:
        return render_template('recovery_data.html', user_role=current_user.role_id, form=form)


@app.route('/remove-event', methods=['GET', 'POST'])
def remove_event():
    utils.check_access()
    form = RemoveEventForm()
    variables = {'id': form.person_id.data}
    user = User.query.filter_by(id=variables['id']).first()

    if user:
        audit_log_data = {
            "person_id": current_user.id,
            "caller": SERVICE_NAME,
            "action": utils.retrieve_function_name(),
            "input": variables
        }
        utils.register_action(audit_log_data)
        events = utils.retrieve_events(user.id, 0)
        return render_template('remove_event.html', user_role=current_user.role_id, events=events, form=form)
    else:
        return render_template('remove_event.html', user_role=current_user.role_id, form=form)


@app.route('/audit-log', methods=['GET', 'POST'])
def audit_log():
    utils.check_access()
    utils.check_read_only_access()
    form = AuditLogForm()
    variables = {'id': form.person_id.data}
    user = User.query.filter_by(id=variables['id']).first()

    if user:
        audit_log_data = {
            "person_id": current_user.id,
            "caller": SERVICE_NAME,
            "action": utils.retrieve_function_name(),
            "input": variables
        }
        utils.register_action(audit_log_data)
        audit_logs = utils.retrieve_audit_logs(user.id)
        return render_template('audit_log.html', user_role=current_user.role_id, audit_logs=audit_logs, form=form)
    else:
        return render_template('audit_log.html', user_role=current_user.role_id, form=form)
