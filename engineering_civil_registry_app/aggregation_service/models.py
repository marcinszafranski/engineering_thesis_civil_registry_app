from sqlalchemy import Sequence
from flask_login import UserMixin
from app import db

class User(db.Model, UserMixin):
    __tablename__ = 'users'
    id = db.Column(db.Integer, Sequence('user_id_seq'), primary_key=True)
    name = db.Column(db.String(64))
    surname = db.Column(db.String(64))
    pesel = db.Column(db.String(11), unique=True, index=True)
    username = db.Column(db.String(64), unique=True, index=True)
    password_hash = db.Column(db.String(128))
    birth_date = db.Column(db.Date)
    role_id = db.Column(db.Integer, nullable=False)
    is_activated = db.Column(db.Integer, default=0)


class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)