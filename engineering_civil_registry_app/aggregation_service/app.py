from __init__ import create_app

app, db, bcrypt, login_manager = create_app()

from auth import *
from routes import *

if __name__ == "__main__":
    app.run(debug=True, port=5001)
