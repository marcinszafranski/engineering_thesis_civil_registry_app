from flask_wtf import FlaskForm
from wtforms import StringField, DateField, PasswordField, SubmitField, IntegerField
from wtforms.validators import InputRequired, Length, Email, ValidationError
from models import User


class UtilsForms:

    @staticmethod
    def validate_unique_username(form, field):
        username = field.data
        existing_user = User.query.filter_by(username=username).first()
        if existing_user:
            raise ValidationError('Login jest już zajęty, proszę wybrać inny.')

    @staticmethod
    def validate_unique_pesel(form, field):
        pesel = field.data
        existing_user = User.query.filter_by(pesel=pesel).first()
        if existing_user:
            raise ValidationError('PESEL jest już zarejestrowany.')

    @staticmethod
    def validate_pesel(form, field):
        pesel = field.data
        sum, ct = 0, [1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1]
        for i in range(11):
            sum += (int(pesel[i]) * ct[i])
        return str(sum)[-1] == '0'


class RegisterForm(FlaskForm):
    username = StringField(validators=[
        InputRequired(), Email(), Length(min=4, max=60), UtilsForms.validate_unique_username], render_kw={"placeholder": "Adres e-mail"})
    name = StringField(validators=[InputRequired(), Length(min=3, max=13)], render_kw={"placeholder": "Imię"})
    surname = StringField(validators=[InputRequired(), Length(min=2, max=51)], render_kw={"placeholder": "Nazwisko"})
    pesel = StringField(validators=[Length(min=11, max=11), UtilsForms.validate_pesel, UtilsForms.validate_unique_pesel],
                        render_kw={"placeholder": "Numer pesel"})
    birth_date = DateField()
    password = PasswordField(validators=[
        InputRequired(), Length(min=8, max=20)], render_kw={"placeholder": "Hasło"})

    submit = SubmitField('Zarejestruj się')


class LoginForm(FlaskForm):
    username = StringField(validators=[
                           InputRequired(), Email(), Length(min=6, max=60)], render_kw={"placeholder": "Adres e-mail"})
    password = PasswordField(validators=[
                             InputRequired(), Length(min=8, max=60)], render_kw={"placeholder": "Hasło"})
    submit = SubmitField('Zaloguj się')


class RemoveEventForm(FlaskForm):
    person_id = IntegerField(validators=[InputRequired()])
    submit = SubmitField('Wyszukaj')


class SearchUsersForm(FlaskForm):
    person_id = IntegerField()
    name_surname = StringField()
    pesel = IntegerField(validators=[UtilsForms.validate_pesel])
    submit = SubmitField('Wyszukaj')


class RecoveryDataForm(FlaskForm):
    person_id = IntegerField(validators=[InputRequired()])
    submit = SubmitField("Wyszukaj")


class AuditLogForm(FlaskForm):
    person_id = IntegerField(validators=[InputRequired()])
    submit = SubmitField("Wyszukaj")
