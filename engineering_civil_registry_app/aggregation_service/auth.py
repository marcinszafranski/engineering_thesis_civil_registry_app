from flask import render_template, url_for, redirect, flash
from flask_login import login_user, login_required, logout_user, current_user
from app import app, db, bcrypt, login_manager
from models import User
from forms import LoginForm, RegisterForm
from utils import Utils

SERVICE_NAME = "AGGREGATION_SERVICE"
utils = Utils(SERVICE_NAME)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@app.route('/')
def entry():
    return redirect(url_for('home'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user:
            if bcrypt.check_password_hash(user.password_hash, form.password.data):
                login_user(user)
                return redirect(url_for('home'))
            else:
                flash('Niepoprawne hasło', 'error')
        else:
            flash('Nie ma użytkownika o takim adresie email', 'error')
    else:
        for field, errors in form.errors.items():
            for error in errors:
                flash(error, 'error')
    return render_template('login.html', form=form)


@app.route('/home', methods=['GET', 'POST'])
@login_required
def home():
    if current_user.is_activated == 0:
        return render_template('account_activation.html', name=current_user.name)
    else:
        user_role = current_user.role_id
        return render_template('home.html', user_role=user_role)


@app.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        existing_user_username = User.query.filter_by(username=form.username.data).first()
        if existing_user_username:
            flash('Login zajęty, skorzystaj z innego.', 'error')
        else:
            password_hash = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
            new_user = User(username=form.username.data, name=form.name.data, surname=form.surname.data,
                            pesel=form.pesel.data, password_hash=password_hash, birth_date=form.birth_date.data)
            db.session.add(new_user)
            db.session.commit()
            flash('Rejestracja przebiegła pomyślnie! Teraz możesz się zalogować.', 'success')
            return redirect(url_for('login'))
    else:
        for field, errors in form.errors.items():
            for error in errors:
                flash(error, 'error')
    return render_template('register.html', form=form)