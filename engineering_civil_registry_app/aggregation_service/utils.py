from flask_login import current_user
from flask import abort, request
import requests
import inspect


class Utils:

    def __init__(self, service_name):
        self.SERVICE_NAME = service_name
        self.BASE_URLS = {
            'fetch_person_info': {
                'url': "http://localhost:5003/get-info",
                'log_audit': True
            },
            'fetch_event_types': {
                'url': "http://localhost:5002/get-event-names",
                'log_audit': True
            },
            'fetch_events': {
                'url': "http://localhost:5002/get-event",
                'log_audit': True
            },
            'fetch_audit_logs': {
                'url': "http://localhost:5004/get-logs",
                'log_audit': False
            },
            'log_audit': {
                'url': "http://localhost:5004/log_audit",
                'log_audit': False
            }
        }

    def _log_audit(self, action, variables):
        audit_log_data = {
            "person_id": current_user.id,
            "caller": self.SERVICE_NAME,
            "action": action,
            "input": variables
        }
        self.make_request('log_audit', json=audit_log_data)

    def register_action(self, to_be_registered):
        self.make_request('log_audit', json=to_be_registered)

    def make_request(self, action, params=None, json=None):
        base_url_info = self.BASE_URLS.get(action)
        if not base_url_info:
            raise ValueError("Invalid action")

        url = base_url_info['url']
        log_audit = base_url_info.get('log_audit', False)

        if json:
            response = requests.post(url, json=json)
        else:
            response = requests.get(url, params=params)

        status_code = response.status_code
        variables = (json or params).copy() if json or params else {}
        variables['status_code'] = status_code

        if log_audit:
            self._log_audit(action, variables)

        return response

    @staticmethod
    def retrieve_function_name():
        frame = inspect.currentframe().f_back
        return frame.f_code.co_name

    def retrieve_person_info(self, person_id, fields):
        params = {'person_id': person_id, 'fields': ','.join(fields)}
        response = self.make_request('fetch_person_info', params=params)
        if response.status_code == 200:
            return response.json()
        else:
            return "unknown person id"

    def retrieve_event_types(self):
        response = self.make_request('fetch_event_types')
        if response.status_code == 200:
            return response.json()
        else:
            return "something went wrong"

    def generate_person_info(self, person_id, person2_id):
        person_info = dict(self.retrieve_person_info(person_id, ['id', 'name', 'surname', 'pesel']))
        if person2_id:
            person2_info = dict(self.retrieve_person_info(person2_id, ['id', 'name', 'surname', 'pesel']))
        else:
            person2_info = {}
        for key in list(person2_info):
            person2_info[f'spouse_{key}'] = person2_info.pop(key)
        person_info.update(person2_info)
        return person_info

    def retrieve_events(self, person_id, archived_events=False):
        params = {'person_id': person_id}
        if archived_events:
            params['archived_events'] = archived_events
        response = self.make_request('fetch_events', params=params)
        if response.status_code == 200:
            data = response.json()
            event_type_names = dict(self.retrieve_event_types())
            person_data = {}
            events = []
            if data:
                for event in data:
                    event_type_id = str(event['event_type_id'])
                    event.update({'event_name': event_type_names[event_type_id]})
                    person_id = event['person_id']
                    person2_id = event['person2_id']

                    if person_data and person2_id is None:
                        if person_id == person_data['id']:
                            person_info = {}
                            for k in ['id', 'name', 'surname', 'pesel']:
                                person_info[k] = person_data[k]
                            event.update(person_info)
                        else:
                            person_data = self.generate_person_info(person_id, person2_id)
                            event.update(person_data)
                    else:
                        person_data = self.generate_person_info(person_id, person2_id)
                        event.update(person_data)

                    events.append(event)
                return events
            else:
                person_info = dict(self.retrieve_person_info(person_id, ['id', 'name', 'surname', 'pesel']))

                events.append(person_info)
                return events

        else:
            return 'something went wrong', 404

    def retrieve_audit_logs(self, person_id):
        params = {'person_id': person_id}
        response = self.make_request('fetch_audit_logs', params=params)
        if response.status_code == 200:
            data = response.json()
            person_data = []
            audit_logs = []
            if data:
                for audit_log in data:
                    person_id = audit_log['person_id']
                    person_info = {}

                    if person_data:
                        for person in person_data:
                            if person_id in person:
                                audit_log.update(person[person_id])
                    else:
                        person_info = dict(self.retrieve_person_info(person_id, ['name', 'surname', 'id']))
                        person_data.append({person_id: person_info})
                    audit_log.update(person_info)

                    audit_logs.append(audit_log)
                return audit_logs
        else:
            return 'something went wrong', 404

    @staticmethod
    def check_read_only_access():
        restricted_routes = ['/recovery-data', '/access-control', '/search-users',
                             '/audit-log']
        if current_user.role_id == 2 and request.path in restricted_routes \
                                    and request.method in ['POST', 'PUT', 'DELETE']:
            abort(403)

    @staticmethod
    def check_access():
        if current_user.role_id not in [2, 3]:
            abort(403)
