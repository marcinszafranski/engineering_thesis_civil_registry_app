from flask import jsonify, send_file, abort
from models import Event
from io import BytesIO


class Utils:

    @staticmethod
    def event_validity(db, event_id, archive):
        words = {
            0: "un",
            1: ""
        }
        event = db.session.query(Event).get(event_id)
        if event:
            event.is_archived = archive
            db.session.commit()
            return jsonify({'message': f'Event {words[archive]}archive successfully'}), 200
        else:
            return jsonify({'message': 'Event not found'}), 404

    @staticmethod
    def get_document(db, document_model, document_id):
        document = db.session.query(document_model).get(document_id)
        if document:
            file_obj = BytesIO(document.document_scan)
            return send_file(file_obj, mimetype='application/pdf')
        else:
            return jsonify({'message': 'Document not found'}), 404

    @staticmethod
    def get_event_names(event_types):
        event_type_names = {}
        for event_type in event_types:
            event_type_names.update({event_type.event_type_id: event_type.event_type_name})
        return jsonify(event_type_names), 200
