from __init__ import create_app

app, db = create_app()

from routes import *

if __name__ == '__main__':
    app.run(port=5002)
