from app import db





class EventType(db.Model):
    __tablename__ = 'event_types'
    event_type_id = db.Column(db.Integer, primary_key=True)
    event_type_name = db.Column(db.String(100))


class Document(db.Model):
    __tablename__ = 'documents'
    document_id = db.Column(db.Integer, primary_key=True)
    document_title = db.Column(db.String(100))
    document_scan = db.Column(db.LargeBinary)


class Event(db.Model):
    __tablename__ = 'events'
    event_id = db.Column(db.Integer, primary_key=True)
    event_type_id = db.Column(db.Integer)
    person_id = db.Column(db.Integer)
    event_date = db.Column(db.DateTime)
    description = db.Column(db.String(200))
    is_archived = db.Column(db.Integer)
    document_id = db.Column(db.Integer)
    person2_id = db.Column(db.Integer)
