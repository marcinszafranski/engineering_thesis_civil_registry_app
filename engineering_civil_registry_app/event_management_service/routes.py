from flask import request, jsonify, send_file
from datetime import datetime
from app import app, db
from models import Event, Document, EventType
from io import BytesIO
from utils import Utils


@app.route('/create-event', methods=['POST'])
def create_event():
    data = request.form  # Use request.form to access form data fields
    event_type_id = data.get('event_type_id')
    person_id = data.get('person_id')
    event_date = data.get('event_date')
    description = data.get('event_description')
    second_person_id = data.get('second_person_id')
    event_date = datetime.strptime(event_date, '%Y-%m-%dT%H:%M')

    event = Event(event_type_id=event_type_id,
                  person_id=person_id,
                  event_date=event_date,
                  description=description,
                  person2_id=second_person_id,
                  is_archived=0
                  )

    db.session.add(event)
    db.session.commit()

    if 'event_document' in request.files:
        document_file = request.files['event_document']
        if document_file.filename != '':
            document = Document(document_scan=document_file.read())
            db.session.add(document)
            db.session.flush()
            event.document_id = document.document_id

    db.session.commit()

    return jsonify({'message': 'Event created successfully'}), 200


@app.route("/archive-event", methods=['GET'])
def archive_event():
    event_id = request.args.get('event_id')
    return Utils.event_validity(db, event_id, 1)


@app.route("/unarchive-event", methods=['GET'])
def unarchive_event():
    event_id = request.args.get('event_id')
    return Utils.event_validity(db, event_id, 0)


@app.route("/get-event", methods=['GET'])
def get_events():
    person_id = request.args.get('person_id')
    archived_events = request.args.get('archived_events')
    if archived_events == 1:
        events = Event.query.filter(
            (Event.person_id == person_id) | (Event.person2_id == person_id),
            Event.is_archived == archived_events
        ).all()
    elif archived_events == 10:
        events = Event.query.filter(
        (Event.person_id == person_id) | (Event.person2_id == person_id)
        ).all()
    else:
        events = Event.query.filter(
            (Event.person_id == person_id) | (Event.person2_id == person_id),
            Event.is_archived == 0
        ).all()
    result = []
    for event in events:
        event_data = {
            "event_id": event.event_id,
            "event_type_id": event.event_type_id,
            "person_id": event.person_id,
            "person2_id": event.person2_id,
            "event_date": event.event_date.strftime('%Y-%m-%d %H:%M:%S'),
            "description": event.description,
            "is_archived": event.is_archived,
            "document_id": event.document_id
        }
        result.append(event_data)
    return jsonify(result), 200


@app.route('/get-document', methods=['GET'])
def get_document_route():
    document_id = request.args.get('document_id')
    document = Document.query.get(document_id)

    if document:
        file_obj = BytesIO(document.document_scan)
        return send_file(file_obj, mimetype='application/pdf')
    else:
        return jsonify({'message': 'Document not found'}), 404


@app.route('/get-event-names', methods=['GET'])
def get_event_names_route():
    event_types = EventType.query.all()
    return Utils.get_event_names(event_types)
