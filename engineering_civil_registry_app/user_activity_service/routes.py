from flask import request, jsonify
from app import app, db
from models import User


@app.route('/activate-user', methods=['GET', 'POST'])
def activate_user():
    person_id = request.args.get('person_id')
    user = User.query.get(person_id)

    if user:
        user.is_activated = True
        db.session.commit()
        return jsonify({'message': 'User activation successful'})
    else:
        return jsonify({'message': 'User not located'})


@app.route('/change-role', methods=['GET', 'POST'])
def change_role():
    person_id = request.args.get('person_id')
    user_permissions = request.args.get('user_permissions')
    user = User.query.get(person_id)

    if user:
        user.role_id = user_permissions
        db.session.commit()
        return jsonify({'message': 'User access role updated'})
    else:
        return jsonify({'message': 'User not found'})


@app.route('/get-info', methods=['GET', 'POST'])
def get_info():
    person_id = request.args.get('person_id')
    fields = request.args.get('fields').split(',')
    user = User.query.get(person_id)
    response = {}

    if user:
        for field in fields:
            response[field] = getattr(user, field, None)

    return jsonify(response)
